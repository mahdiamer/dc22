---
title: Facilitated visa application process
---

From Debconf22 visa team,

If you need a visa to enter Kosovo to join
[DebConf22](https://debconf22.debconf.org), and want local organizer
[FLOSSK](https://flossk.org) to facilitate this process, please e-mail
[visa@debconf.org](mailto:visa@debconf.org) with a copy of your passport,
latest by May 1st. Do this even if you are not completely sure you will be
applying.

Following the distance application approval in Kosovo, FLOSSK will provide you
with an invitation letter, which you may use when applying for the visa to one
of the 2 consulates available.

Note that you will need to send your passport via post either in New York or
Istanbul to get your visa stamped, so please also tell us which consulate you
will be applying to.

Please refer to the [visa page](https://debconf22.debconf.org/about/visas) for
the complete process and required documents.

We are  here if you have any questions.

[DebConf22](https://debconf22.debconf.org) **will take place from July 17th to
24th, 2022 at the [Innovation and Training Park (ITP)](https://itp-prizren.com)
in Prizren, Kosovo**, and will be preceded by DebCamp, from July 10th to 16th.


