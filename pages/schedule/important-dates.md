---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>


| **MARCH**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 7 March               | Decision on having the physical conference                           |
| 17 March              | Opening of the [Call For Proposals](/cfp/)                           |
| 17 March              | Opening attendee registration                                        |

| **MAY**               |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 1 May (Sunday)        | Last date to apply for a bursary.                                    |
| 1 May (Sunday)        | Last day for submitting a talk that will be considered for the official schedule. |
| 1 May (Sunday)        | Last date to let us know if you need a visa to Kosovo (email: visa@debconf.org with a copy of your passport)                                    |

| **JUNE**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 20 June               | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |

| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| *DebCamp*             |                                                                      |
| 10 (Sunday)           | First day of DebCamp / arrival day for DebCamp                       |
| 11 (Monday)           | Second day of DebCamp                                                |
| 12 (Tuesday)          | Third day of DebCamp                                                 |
| 13 (Wednesday)        | Fourth day of DebCamp                                                |
| 14 (Thursday)         | Fifth day of DebCamp                                                 |
| 15 (Friday)           | Sixth day of DebCamp                                                 |
| 16 (Saturday)         | Seventh day of DebCamp / arrival day for DebConf                     |
| *DebConf*             |                                                                      |
| 17 (Sunday)           | First day of DebConf / opening                                       |
| 18 (Monday)           | Second day of DebConf                                                |
| 19 (Tuesday)          | Third day of DebConf / cheese and wine party                         |
| 20 (Wednesday)        | Fourth day of DebConf                                                |
| 21 (Thursday)         | Fifth day of DebConf                                                 |
| 22 (Friday)           | Sixth day of DebConf / conference dinner                             |
| 23 (Saturday)         | Seventh day of DebConf                                               |
| 24 (Sunday)           | Last day of DebConf / closing ceremony / barbecue party              |
| 25 (Monday)           | Departure day                                                        |
