---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. [DebConf19][] (the last
in-person DebConf) took place in Curitiba, Brazil and was attended by
382 participants from 50 countries.

[DebConf19]: https://debconf19.debconf.org/

**DebConf22 is taking place in Prizren, Kosovo from July 17 to July 24, 2022.**

It is being preceded by DebCamp, from July 10 to July 16, 2022.

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

<br />
## Venue

The [Innovation and Training Park (ITP)](https://itp-prizren.com/) in
Prizren is a business park located in the Eastern suburb of the city of
Prizren.
The area of 39 hectares offers a networking atmosphere which builds on
collaboration and shared resources.
The [venue](../venue/) offers several conference halls and classrooms for
plenaries, presentations, discussions, and informal workshops.
[Accommodation](../accommodation/) for participants is available in
hotels nearby.

<br />
## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.

We look forward to seeing you in Kosovo!
